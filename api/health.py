import json
import requests

from .helper import process_request, generate_uuid
from .logger import get_logger

logger = get_logger()


def event_handler(event, context):
    return process_request(event, health)


def health(event):
    logger.info("Hitting endpoint")
    logger.info("{}".format(event))

    resp = requests.get("http://ec2-3-86-24-200.compute-1.amazonaws.com/basic/actuator/health")

    # request_data = json.loads(event['body'])

    response = {
        "statusCode": 201,
        "body": resp.json()
    }
    return response
