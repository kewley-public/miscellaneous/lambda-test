""" Provides a logger that can be shared across modules with single initialization, preventing duplicate log statements """
import logging
import sys

logger = None


def get_logger():
    global logger

    if logger is not None:
        return logger
    else:
        logger = logging.getLogger('Test Logger')
        logger.setLevel(logging.INFO)
        logger.propagate = False

        handler = logging.StreamHandler(sys.stdout)
        handler.setLevel(logging.INFO)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        handler.setFormatter(formatter)
        logger.addHandler(handler)

        return logger
