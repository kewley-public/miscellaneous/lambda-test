import requests

from .helper import process_request
from .logger import get_logger

logger = get_logger()


def event_handler(event, context):
    logger.info("{}".format(context))
    return process_request(event, another)


def another(event):
    logger.info("{}".format(event))
    resp = requests.get("http://ec2-3-86-24-200.compute-1.amazonaws.com/basic/actuator/health")
    return {
        "statusCode": 201,
        "body": resp.json()
    }
