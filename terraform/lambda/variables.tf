variable "app_name" {
  description = "The application name"
}

variable "environment" {
  description = "The environment"
}

variable "s3_bucket_name" {
  description = "The S3 bucket name that holds our api"
}

variable "s3_api_key_map" {
  description = "A mapping of s3 keys to the corresponding apis"
  type = "map"
}

variable "vpc_id" {
  description = "The VPC Id"
}

variable "subnets" {
  description = "The subnet id's for the lambda functions"
  type = "list"
}