# ---------------------------------------------------------------------------------------------------------------------
# IAM role which dictates what other AWS services the Lambda function may access.
# ---------------------------------------------------------------------------------------------------------------------
resource "aws_iam_role" "lambda_exec" {
  name = "${var.app_name}-api-lambda-role-${var.environment}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags {
    Environment = "${var.environment}"
    Owner = "${var.app_name}"
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# Policy Document for Logging
# ---------------------------------------------------------------------------------------------------------------------
data "aws_iam_policy_document" "lambda_logging_document" {
  statement {
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup"
    ]
    resources = [
      "arn:aws:logs:us-east-1:747313434235:*"
    ]
  }

  statement {
    effect = "Allow"
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ],
    resources = [
      "arn:aws:logs:us-east-1:747313434235:log-group:/aws/lambda/myLambda:*"
    ]
  }
}

resource "aws_iam_policy" "lambda_logging_policy" {
  policy = "${data.aws_iam_policy_document.lambda_logging_document.json}"
  name = "${var.app_name}-lambda-logging-policy-${var.environment}"
}

# ---------------------------------------------------------------------------------------------------------------------
# Policy Document for S3 access
# ---------------------------------------------------------------------------------------------------------------------
data "aws_iam_policy_document" "lambda_s3_document" {
  statement {
    effect = "Allow"
    actions = [
      "s3:Get*",
      "s3:List*"
    ],
    resources = [
      "arn:aws:s3:::${var.s3_bucket_name}/*",
    ]
  }
}

resource "aws_iam_policy" "lambda_s3_iam_policy" {
  policy = "${data.aws_iam_policy_document.lambda_s3_document.json}"
  name = "${var.app_name}-lambda-s3-policy-${var.app_name}"
}

# ---------------------------------------------------------------------------------------------------------------------
# Policy to allow Lambda to access EC2 instances
# ---------------------------------------------------------------------------------------------------------------------
resource "aws_iam_role_policy_attachment" "AWSLambdaVPCAccessExecutionRole" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
  role = "${aws_iam_role.lambda_exec.name}"
}

# ---------------------------------------------------------------------------------------------------------------------
# Policy to allow Lambda to create logging
# ---------------------------------------------------------------------------------------------------------------------
resource "aws_iam_role_policy_attachment" "lambda_logging_policy_attachment" {
  policy_arn = "${aws_iam_policy.lambda_logging_policy.arn}"
  role = "${aws_iam_role.lambda_exec.name}"
}

# ---------------------------------------------------------------------------------------------------------------------
# Policy to allow Lambda to access the S3 buckets
# ---------------------------------------------------------------------------------------------------------------------
resource "aws_iam_role_policy_attachment" "lambda_s3_policy_attachment" {
  policy_arn = "${aws_iam_policy.lambda_s3_iam_policy.arn}"
  role = "${aws_iam_role.lambda_exec.name}"
}

# ---------------------------------------------------------------------------------------------------------------------
# Security group to attach to the lambda and the EC2 instance so the lambda may talk to the EC2
# ---------------------------------------------------------------------------------------------------------------------
resource "aws_security_group" "sg_lambda" {
  name = "${var.app_name}-api-sg-${var.environment}"
  description = "${var.app_name} Security group for lambda on environment ${var.environment}"
  vpc_id = "${var.vpc_id}"

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.app_name}-api-sg-${var.environment}"
    Environment = "${var.environment}"
    Owner = "${var.app_name}"
  }
}


# ---------------------------------------------------------------------------------------------------------------------
# API DEFINITIONS BELOW
# ---------------------------------------------------------------------------------------------------------------------

# ---------------------------------------------------------------------------------------------------------------------
# Test API
# ---------------------------------------------------------------------------------------------------------------------
resource "aws_lambda_function" "test_api" {
  function_name = "${var.app_name}-test-${var.environment}"

  # The bucket name as created earlier with "aws s3api create-bucket"
  s3_bucket = "${var.s3_bucket_name}"
  s3_key    = "${var.s3_api_key_map["test"]}"

  # "main" is the filename within the zip file (main.js) and "handler"
  # is the name of the property under which the handler function was
  # exported in that file.
  handler = "api.health.event_handler"
  runtime = "python3.7"

  role = "${aws_iam_role.lambda_exec.arn}"
  vpc_config {
    security_group_ids = ["${aws_security_group.sg_lambda.id}"]
    subnet_ids = "${var.subnets}"
  }

  tags {
    Environment = "${var.environment}"
    Owner = "${var.app_name}"
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# Test API AppSync Roles so AppSync may communicate with our lambda
# ---------------------------------------------------------------------------------------------------------------------
resource "aws_iam_role" "appsync_test_api_role" {
  name = "${var.app_name}-appsync-test-api-role-${var.environment}"
  description = "Allows the AWS AppSync service to access your data source"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "appsync.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags {
    Environment = "${var.environment}"
    Owner = "${var.app_name}"
  }
}

data "aws_iam_policy_document" "test_api_access_policy_document" {
  statement {
    effect = "Allow"
    actions = [
      "lambda:invokeFunction"
    ],
    resources = [
      "${aws_lambda_function.test_api.arn}",
      "${aws_lambda_function.test_api.arn}:*",
    ]
  }
}

resource "aws_iam_policy" "test_api_acccess_policy" {
  policy = "${data.aws_iam_policy_document.test_api_access_policy_document.json}"
  name = "${var.app_name}-test-api-access-policy-${var.environment}"
}

resource "aws_iam_role_policy_attachment" "test_api_policy_attachment" {
  policy_arn = "${aws_iam_policy.test_api_acccess_policy.arn}"
  role = "${aws_iam_role.appsync_test_api_role.name}"
}

# ---------------------------------------------------------------------------------------------------------------------
# Another API
# ---------------------------------------------------------------------------------------------------------------------
resource "aws_lambda_function" "another_api" {
  function_name = "${var.app_name}-another-${var.environment}"

  # The bucket name as created earlier with "aws s3api create-bucket"
  s3_bucket = "${var.s3_bucket_name}"
  s3_key    = "${var.s3_api_key_map["test"]}"

  # "main" is the filename within the zip file (main.js) and "handler"
  # is the name of the property under which the handler function was
  # exported in that file.
  handler = "api.another.event_handler"
  runtime = "python3.7"

  role = "${aws_iam_role.lambda_exec.arn}"
  vpc_config {
    security_group_ids = ["${aws_security_group.sg_lambda.id}"]
    subnet_ids = "${var.subnets}"
  }

  tags {
    Environment = "${var.environment}"
    Owner = "${var.app_name}"
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# Another API AppSync Roles so AppSync may communicate with our lambda
# ---------------------------------------------------------------------------------------------------------------------
resource "aws_iam_role" "appsync_test_another_api_role" {
  name = "${var.app_name}-appsync-test-another-api-role-${var.environment}"
  description = "Allows the AWS AppSync service to access your data source"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "appsync.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags {
    Environment = "${var.environment}"
    Owner = "${var.app_name}"
  }
}

data "aws_iam_policy_document" "test_another_api_access_policy_document" {
  statement {
    effect = "Allow"
    actions = [
      "lambda:invokeFunction"
    ],
    resources = [
      "${aws_lambda_function.another_api.arn}",
      "${aws_lambda_function.another_api.arn}:*",
    ]
  }
}

resource "aws_iam_policy" "test_another_api_acccess_policy" {
  policy = "${data.aws_iam_policy_document.test_another_api_access_policy_document.json}"
  name = "${var.app_name}-test-another-api-access-policy-${var.environment}"
}

resource "aws_iam_role_policy_attachment" "test_another_api_policy_attachment" {
  policy_arn = "${aws_iam_policy.test_another_api_acccess_policy.arn}"
  role = "${aws_iam_role.appsync_test_another_api_role.name}"
}