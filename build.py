import os
from shutil import rmtree, copytree
from subprocess import call


def ignore(dir, files):
    return ["requirements.txt", "README.md", "terraform", ".gitlab-ci.yml", ".gitignore", ".idea", "build.py", ".git"]


# Creates the deployment packages for the lambda functions including dependencies

if __name__ == "__main__":
    # check to see if build directory exists
    if os.path.exists("./build"):
        # clear it out if it does
        rmtree("./build")

    # copy project packages to build directory, excluding the files specified in ignore()
    copytree(".", "./build", ignore=ignore)

    # install pip dependencies
    # call('pip3 install -i https://%s:%s@nexus-cloud.windlogics.com/repository/pypi-all/simple -r ../requirements.txt -t .' % (
    #     os.getenv('nexusUsername'), os.getenv('nexusPassword')), shell=True, cwd="./build")

    call('pip install -r ../requirements.txt -t .', shell=True, cwd='./build')

    # ZIP the deployment package and put it back into the root pipeline dir for the cloudformation deploy
    call('zip -r test-api-project.zip .', shell=True, cwd="./build")
    call('mv build/test-api-project.zip .', shell=True, cwd=".")
